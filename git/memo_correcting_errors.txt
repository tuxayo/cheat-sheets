# Undo last commit and changes will be staged
git reset --soft 'HEAD^'

# Undo last commit and changes reverted
git reset --hard 'HEAD^'

# Undo last commit on remote repo
git push -f origin HEAD^:master

# For a collaborator: Sync after a forced push
git reset origin/master --hard

# Undo last commit on remote repo bis
git rebase -i HEAD~2	#Si il faut undo en local
# supprimer la 2ème ligne dans l'éditeur de texte
git push origin +master	#force push pour undo sur le dépot

# handle a conflict because of a renamed and edited file on the two branches
- rollback the merge
git merge myBranch -X rename-threshold=25

# rollback a conflicting merge
# after after a merge: "OMG so much conflicts I cant solve that mess, ABORT!!!"
git reset --merge ORIG_HEAD

# Add file to last commit
git add myFile
git commit --amend -m "Updated message"

git reset myFile # remove a file from the staging area (unstage)
git checkout -- myFile # revert myFile changes from last commit
git rm myFile # delete myFile and stage the removal # -r works
git rm --cached myFile # keep myFile, stop tracking and stage the removal
# put as it was on a commit (revert changes from f6181b051)
# works also with a branch name
git checkout f6181b051 -- myFile
