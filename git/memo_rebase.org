* Principle
Usefull to not pollute the log with merge and conflict solving commits
git rebase otherBranch
roll back current branch at fork time
runs otherBranch's commits on current branch
runs commits since fork on current branch


* conflicts
** solve it manualy and
git rebase --continue
** or skip the conflicting commit
git rebase --skip
** or rollback before the rebase
git rebase --abort

* Changer l'ordre, fusionner deux commits et edit un comit qui n'est pas le dernier
** 5 derniers commit
git rebase -i HEAD~5 
** dans l'editeur
edit et squash et changer l'order
** save and quit launch the process
** edit old commit
git add foo.bar
git commit --amend 
** continue
git rebase --continue
